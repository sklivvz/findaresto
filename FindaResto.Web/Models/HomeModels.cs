﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FindaResto.Core;

namespace FindaResto.Web.Models
{
    /// <summary>
    /// The model used by the current view of a restaurant
    /// </summary>
    public class RestaurantView
    {
        public string Name { get; set; }
        public decimal AverageRating { get; set; }
        public Uri LogoUri { get; set; }
        public string RestaurantUri { get; set; }
    }

    /// <summary>
    /// A list of restaurants as a model used by the view.
    /// </summary>
    public class RestaurantListView : List<RestaurantView>
    {
        public RestaurantListView(IEnumerable<IRestaurant> source)
        {
            AddRange(source.Select(r => new RestaurantView
                                            {
                                                Name = r.Name,
                                                AverageRating = decimal.Parse(r.AvgRating == "" ? "0" : r.AvgRating),
                                                RestaurantUri = r.URL,
                                                LogoUri = new Uri(
                                                    string.Format(CultureInfo.InvariantCulture,
                                                                  "http://www.just-eat.co.uk/images/restaurants/{0}.gif",
                                                                  r.RestaurantId)), 
                                                //HACK: there is no guarantee (formally) that this URI will be valid.
                                                //However, we don't want to make a webservice call per line, so let's KISS.
                                            }));
        }
    }
}