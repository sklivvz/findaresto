﻿using System;
using System.Linq;
using System.Web.Mvc;
using FindaResto.Core;
using FindaResto.Web.Models;

namespace FindaResto.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRestaurantLocator _locator;

        /// <summary>
        /// Default constructor
        /// </summary>
        public HomeController() : this(new RestaurantLocator())
        {
        }

        /// <summary>
        /// Constructor with Dependancy Injection
        /// </summary>
        /// <param name="locator">The <c>IRestaurantLocator</c> to be used by the controller</param>
        public HomeController(IRestaurantLocator locator)
        {
            if (locator == null) throw new ArgumentNullException("locator");
            _locator = locator;
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Home/Find/

        public ActionResult Find(string postcode)
        {
            //As per requirement, restaurants are sorted by rating
            var res = new RestaurantListView(
                _locator
                    .GetRestaurantsByPostcode(postcode)
                    .OrderByDescending(x => decimal.Parse(x.AvgRating == "" ? "0" : x.AvgRating))
                );

            return View(res);
        }
    }
}