using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using FindaResto.Core.JustEat;

namespace FindaResto.Core
{
    /// <summary>
    /// Provides a restaurant finding service to the website
    /// </summary>
    public class RestaurantLocator : IRestaurantLocator
    {
        private readonly webservicesSoap _restaurantWebService;

        /// <summary>
        /// Default constructor, no mock injection
        /// </summary>
        public RestaurantLocator()
            : this(new webservicesSoapClient("webservicesSoap"))
        {
        }

        /// <summary>
        /// Alternate constructor which allows to inject a mock
        /// </summary>
        /// <param name="restaurantWebService">A <c>IRestaurandWebService</c> that provides connectivity to the remote service, if the default is not applicable.</param>
        public RestaurantLocator(webservicesSoap restaurantWebService)
        {
            if (restaurantWebService == null) throw new ArgumentNullException("restaurantWebService");
            _restaurantWebService = restaurantWebService;
        }

        #region IRestaurantLocator Members

        /// <summary>
        /// Returns a list of <c>Restaurant</c> by looking up a partial postcode
        /// </summary>
        /// <param name="postcode">The partial postcode</param>
        /// <returns>A list of restaurants if the postcode is valid, and the provider knows restaurants in the area, an empty collection otherwise.</returns>
        public IEnumerable<IRestaurant> GetRestaurantsByPostcode(string postcode)
        {
            getRestaurantListResponse res =
                _restaurantWebService.getRestaurantList(new getRestaurantListRequest(postcode));
            if (res != null)
            {
                XmlNode node = res.getRestaurantListResult;
                var serializer = new XmlSerializer(typeof (Restaurants));
                string xmlText = node.OuterXml;
                var textReader = new StringReader(xmlText);
                var coll = (Restaurants) serializer.Deserialize(textReader);
                return coll.Items;
            }
            return new Restaurant[] {};
        }

        #endregion
    }
}