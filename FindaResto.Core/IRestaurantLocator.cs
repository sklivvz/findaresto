using System.Collections.Generic;

namespace FindaResto.Core
{
    /// <summary>
    /// Encapsulates a service to locate restaurants
    /// </summary>
    public interface IRestaurantLocator
    {
        /// <summary>
        /// Returns a list of <c>Restaurant</c> by looking up a partial postcode
        /// </summary>
        /// <param name="postcode">The partial postcode</param>
        /// <returns>A list of restaurants if the postcode is valid, and the provider knows restaurants in the area, an empty collection otherwise.</returns>
        IEnumerable<IRestaurant> GetRestaurantsByPostcode(string postcode);
    }
}