using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FindaResto.Core
{
    /// <remarks/>
    [GeneratedCode("xsd", "4.0.30319.1")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Restaurants
    {
        private Restaurant[] _items;

        /// <remarks/>
        [XmlElement("Restaurant", Form = XmlSchemaForm.Unqualified)]
        public Restaurant[] Items
        {
            get { return _items; }
            set { _items = value; }
        }
    }
}