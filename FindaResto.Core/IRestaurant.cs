namespace FindaResto.Core
{
    /// <summary>
    /// A Just-Eat restaurant DTO
    /// </summary>
    public interface IRestaurant
    {
        /// <summary>
        /// The Just-Eat restaurant id
        /// </summary>
        string RestaurantId { get; }

        /// <summary>
        /// The name of the restaurant
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The address of the restaurant
        /// </summary>
        string Address { get; }

        /// <summary>
        /// The postcode of the restaurant
        /// </summary>
        string Postcode { get; }

        /// <summary>
        /// One of the food types of the restaurant
        /// </summary>
        string Foodtype1 { get; }

        /// <summary>
        /// The other food type of the restaurant
        /// </summary>
        string Foodtype2 { get; }

        /// <summary>
        /// The restaurant rating
        /// </summary>
        string AvgRating { get; }

        /// <summary>
        /// The Just Eat Uri of the restaurant
        /// </summary>
        string URL { get; }

        /// <summary>
        /// <c>True</c> if the restaurant is open, otherwise <c>False</c>.
        /// </summary>
        string Open { get; }
    }
}