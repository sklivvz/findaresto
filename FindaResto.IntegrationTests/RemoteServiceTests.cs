﻿using System.Xml;
using FindaResto.Core.JustEat;
using NUnit.Framework;

namespace FindaResto.IntegrationTests
{
    [TestFixture]
    public class RemoteServiceTests
    {
        [Test]
        [TestCase("OX1", false)]
        [TestCase("OX14", false)]
        [TestCase("Foobar", true)]
        public void JustEatHealthCheck(string postcode, bool shouldBeEmpty)
        {
            //Arrange
            var svc = new webservicesSoapClient("webservicesSoap");

            //Act
            XmlNode restNodes = svc.getRestaurantList(new getRestaurantListRequest(postcode)).getRestaurantListResult;

            //Assert
            if (shouldBeEmpty)
            {
                Assert.AreEqual(1, restNodes.ChildNodes.Count);
                Assert.AreEqual(string.Empty, restNodes.ChildNodes[0].InnerXml);
            }
            else
            {
                Assert.True(restNodes.HasChildNodes);
                Assert.AreNotEqual(string.Empty, restNodes.ChildNodes[0].InnerXml);
            }
        }

        [Test]
        public void JustEatIsAvailable()
        {
            //Arrange
            var svc = new webservicesSoapClient("webservicesSoap");

            //Act
            XmlNode ret = svc.getRestaurantList(new getRestaurantListRequest("foobar")).getRestaurantListResult;

            //Assert
            Assert.IsNotNull(ret);
        }
    }
}