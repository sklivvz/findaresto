﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using FindaResto.Core;
using FindaResto.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace FindaResto.UnitTests
{
    [TestFixture]
    internal class HomeControllerTests
    {
        private IRestaurant MockRest(string name, decimal rating, out Mock<IRestaurant> mock)
        {
            mock = new Mock<IRestaurant>();
            mock.SetupGet(x => x.Name).Returns(name).Verifiable();
            mock.SetupGet(x => x.AvgRating).Returns(rating.ToString(CultureInfo.InvariantCulture)).Verifiable();
            return mock.Object;
        }

        private Mock<IRestaurantLocator> SetupMock(out Mock<IRestaurant> mock3, out Mock<IRestaurant> mock4,
                                                   out Mock<IRestaurant> mock1, out Mock<IRestaurant> mock2)
        {
            var mockService = new Mock<IRestaurantLocator>();
            mockService.Setup(x => x.GetRestaurantsByPostcode("OX1")).Returns(new List<IRestaurant>
                                                                                  {
                                                                                      MockRest("Poor", 1, out mock1),
                                                                                      MockRest("Medium", 2, out mock2),
                                                                                      MockRest("Good", 4.44M, out mock3),
                                                                                      MockRest("Unknown", 0, out mock4)
                                                                                  });
            return mockService;
        }

        [Test]
        public void HasAFind()
        {
            //Arrange
            Mock<IRestaurant> mock1, mock2, mock3, mock4;
            Mock<IRestaurantLocator> mockService = SetupMock(out mock3, out mock4, out mock1, out mock2);
            var cnt = new HomeController(mockService.Object);

            //Act
            ActionResult res = cnt.Find("OX1");

            //Assert
            Assert.IsNotNull(res);
            mock1.Verify(restaurant => restaurant.AvgRating);
            mock2.Verify(restaurant => restaurant.AvgRating);
            mock3.Verify(restaurant => restaurant.AvgRating);
            mock4.Verify(restaurant => restaurant.AvgRating);
        }

        [Test]
        public void HasAnIndex()
        {
            //Arrange
            Mock<IRestaurant> mock1, mock2, mock3, mock4;
            Mock<IRestaurantLocator> mockService = SetupMock(out mock3, out mock4, out mock1, out mock2);
            var cnt = new HomeController(mockService.Object);

            //Act
            ActionResult res = cnt.Index();

            //Assert
            Assert.IsNotNull(res);
        }
    }
}