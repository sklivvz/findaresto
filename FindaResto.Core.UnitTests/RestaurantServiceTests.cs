﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FindaResto.Core;
using FindaResto.Core.JustEat;
using Moq;
using NUnit.Framework;

namespace FindaResto.UnitTests
{
    [TestFixture]
    internal class RestaurantServiceTests
    {
        [Test]
        public void RestaurantServiceParsesCorrectly()
        {
            //Arrange
            const string input = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Restaurants>
  <Restaurant>
    <RestaurantId>1636</RestaurantId>
    <Name>Spice of India</Name>
    <Address>55 Between Towns Road Cowley</Address>
    <Postcode>OX4 3LR</Postcode>
    <Foodtype1>Indian</Foodtype1>
    <Foodtype2>[none]</Foodtype2>
    <AvgRating>4.88</AvgRating>
    <URL>http://spice-of-india-ox4.just-eat.co.uk</URL>
    <Open>True</Open>
  </Restaurant>
  <Restaurant>
    <RestaurantId>2973</RestaurantId>
    <Name>Friends</Name>
    <Address>2 Bath Street</Address>
    <Postcode>OX14 3QH</Postcode>
    <Foodtype1>Italian</Foodtype1>
    <Foodtype2>Turkish</Foodtype2>
    <AvgRating>4.86</AvgRating>
    <URL>http://friends-abingdon.just-eat.co.uk</URL>
    <Open>True</Open>
  </Restaurant>
</Restaurants>";
            var doc = new XmlDocument();
            doc.LoadXml(input);
            XmlNode node = doc.ChildNodes[1]; //ChildNode[0] is the XML declaration

            var mock = new Mock<webservicesSoap>();
            mock.Setup(x => x.getRestaurantList(It.IsAny<getRestaurantListRequest>())).Returns(
                new getRestaurantListResponse(node));

            var svc = new RestaurantLocator(mock.Object);

            //Act
            IEnumerable<IRestaurant> res = svc.GetRestaurantsByPostcode("foo");

            //Assert
            Assert.AreEqual(2, res.Count());
        }
    }
}